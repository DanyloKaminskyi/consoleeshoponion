﻿using System.Collections.Generic;
using EShop.Data;
using EShop.Data.Enums;
using Microsoft.EntityFrameworkCore;

namespace EShop.Repo
{
    /// <summary>
    /// Storage for all entities in the program 
    /// </summary>
    public class ShopDbContext : DbContext
    {
        /// <summary>
        /// Default constructor for  <see cref="ShopDbContext"/>
        /// </summary>
        public ShopDbContext() 
        {
        }
        /// <summary>
        /// Orders entity storage in the program 
        /// </summary>
        public List<Order> Orders = new List<Order>();
        /// <summary>
        /// Products entity storage in the program 
        /// </summary>
        public List<Product> Products = new List<Product>()
        {
            new Product{
                Id=0, Name="Leather jacket",
            },
            new Product{
                Id=1, Name= "Dumbell",
            },
            new Product{
                Id=2, Name= "Hat",
            }
        };
        /// <summary>
        /// Customers entity storage in the program 
        /// </summary>
        public List<Customer> Customers = new List<Customer>
        {
            new Customer{
                Id=0, Email="qwerty@gmail.com",
                Role = RoleEnum.User
            },
            new Customer{
                Id=1, Email="admin@gmail.com",
                Role = RoleEnum.Administrator
            }
        };
    }
}