﻿using System;
using System.Collections.Generic;
using System.Linq;
using EShop.Data;
using EShop.Data.Enums;

namespace EShop.Repo
{
    /// <summary>
    /// Implementation of <see cref="IRepository{T}"/> for <see cref="Customer"/> entity 
    /// </summary>
    public class MockCustomerRepo : IRepository<Customer>
    {
        private readonly ShopDbContext _context;

        /// <summary>
        /// Constructor that receives a <see cref="ShopDbContext"/> object 
        /// </summary>
        public MockCustomerRepo(ShopDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates <see cref="Customer"/> in repository
        /// </summary>
        public void Create(Customer customer)
        {
            _context.Customers.Add(customer);
        }

        /// <summary>
        /// Deletes <see cref="Customer"/> from repository
        /// </summary>
        public void Delete(Customer customer)
        {
            _context.Customers.Remove(customer);
        }

        /// <summary>
        /// Gets all <see cref="Customer"/> entities from repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{T}"/>
        /// </returns>
        public IEnumerable<Customer> GetAll()
        {
            return _context.Customers;
        }

        /// <summary>
        /// Gets  <see cref="Customer"/> by id from repository
        /// </summary>
        /// /// <returns>
        /// <see cref="Customer"/>
        /// </returns>
        public Customer GetById(int id)
        {
            if (_context.Customers.Any(x => x.Id == id)) return _context.Customers.Find(x => x.Id == id);
            else throw new ArgumentException();
        }

        /// <summary>
        /// Updates  <see cref="Customer"/> by id in repository
        /// </summary>
        public void Update(int id, Customer customer)
        {
        }
    }
}