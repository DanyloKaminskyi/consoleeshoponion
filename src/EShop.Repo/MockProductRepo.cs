﻿using System;
using System.Collections.Generic;
using System.Linq;
using EShop.Data;

namespace EShop.Repo
{
    /// <summary>
    /// Implementation of <see cref="IRepository{T}"/> for <see cref="Product"/> entity 
    /// </summary>
    public class MockProductRepo : IRepository<Product>
    {
        /// <summary>
        /// Gives access to "Database"
        /// </summary>
        private readonly ShopDbContext _context;

        /// <summary>
        /// Constructor that receives a <see cref="ShopDbContext"/> object 
        /// </summary>
        public MockProductRepo(ShopDbContext context)
        {
            _context = context;
        }

        /// <summary>
        /// Creates <see cref="Product"/> 
        /// </summary>
        public void Create(Product product)
        {
            _context.Products.Add(product);
        }
        /// <summary>
        /// Deletes <see cref="Product"/> 
        /// </summary>
        public void Delete(Product product)
        {
            _context.Products.Remove(product);
        }

        /// <summary>
        /// Gets all <see cref="Product"/> entities
        /// </summary>
        /// /// /// <returns>
        /// <see cref="IEnumerable{Product}"/>
        /// </returns>
        public IEnumerable<Product> GetAll()
        {
            return _context.Products;
        }

        /// <summary>
        /// Gets <see cref="Product"/> by Id
        /// </summary>
        /// /// <returns>
        /// <see cref="Product"/>
        /// </returns>
        public Product GetById(int id)
        {
            if (_context.Products.Any(x => x.Id == id)) return _context.Products.Single(x => x.Id == id);
            throw new ArgumentException();
        }

        /// <summary>
        /// Updates <see cref="Product"/>
        /// </summary>
        public void Update(int id, Product product)
        {
        }
    }
}