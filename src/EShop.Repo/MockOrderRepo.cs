﻿using System;
using System.Collections.Generic;
using System.Linq;
using EShop.Data;
using EShop.Data.Enums;

namespace EShop.Repo
{
    /// <summary>
    /// Implementation of <see cref="IRepository{T}"/> for <see cref="Order"/> entity 
    /// </summary>
    public class MockOrderRepo : IRepository<Order>
    {
        /// <summary>
        /// Gives access to "Database"
        /// </summary>
        private readonly ShopDbContext _context;

        /// <summary>
        /// Constructor that receives a <see cref="ShopDbContext"/> object 
        /// </summary>
        public MockOrderRepo(ShopDbContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Creates <see cref="Order"/> in repository
        /// </summary>
        public void Create(Order order)
        {
            _context.Orders.Add(order);
            Customer customer = order.Customer;
            var customerToRemove = _context.Customers.First(x => x.Email.Equals(customer.Email));
            _context.Customers.Remove(customerToRemove);
            _context.Customers.Add(customer);

        }

        /// <summary>
        /// Deletes <see cref="Order"/> from repository
        /// </summary>
        public void Delete(Order order)
        {
            _context.Orders.Remove(order);
        }

        /// <summary>
        /// Gets all <see cref="Order"/> entities from repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{T}"/>
        /// </returns>
        public IEnumerable<Order> GetAll()
        {
            return _context.Orders;
        }

        /// <summary>
        /// Gets  <see cref="Order"/> by id from repository
        /// </summary>
        /// /// <returns>
        /// <see cref="Order"/>
        /// </returns>
        public Order GetById(int id)
        {
            if (_context.Orders.Any(x => x.Id == id)) return _context.Orders.Find(x => x.Id == id);
            else throw new ArgumentException();
        }

        /// <summary>
        /// Updates  <see cref="Order"/> by id in repository
        /// </summary>
        public void Update(int id, Order order)
        {
            Order orderFromDb = GetById(id);
            if(orderFromDb is null) throw new ArgumentException();
            orderFromDb.Customer = order.Customer;
            orderFromDb.Product = order.Product;
            orderFromDb.Status = order.Status;
        }
    }
}