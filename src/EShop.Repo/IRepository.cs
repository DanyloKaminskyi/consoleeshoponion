﻿using System.Collections.Generic;

namespace EShop.Repo
{
    /// <summary>
    /// Abstraction for various Repositories
    /// </summary>
    public interface IRepository<T>
    {
        /// <summary>
        /// Gets all T entities from repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{T}"/>
        /// </returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Gets T by id from repository
        /// </summary>
        /// /// <returns>
        /// T
        /// </returns>
        T GetById(int id);

        /// <summary>
        /// Creates T in repository
        /// </summary>
        void Create(T item);

        /// <summary>
        /// Updates T by id in repository
        /// </summary>
        void Update(int id, T item);

        /// <summary>
        /// Deletes T from repository
        /// </summary>
        void Delete(T item);
    }
}