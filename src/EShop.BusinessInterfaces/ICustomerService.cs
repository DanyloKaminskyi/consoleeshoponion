﻿using System.Collections.Generic;
using EShop.BusinessModels;

namespace EShop.BusinessInterfaces
{
    /// <summary>
    /// Interface for Customer services 
    /// </summary>
    public interface ICustomerService
    {
        /// <summary>
        /// Gets all Customers
        /// </summary>
        ///  /// <returns>
        /// <see cref="IEnumerable{CustomerDto}"/>
        /// </returns>
        IEnumerable<CustomerDto> GetAllCustomers();


        /// <summary>
        /// Gets Customer by Id
        /// </summary>
        ///  /// <returns>
        /// <see cref="CustomerDto"/>
        /// </returns>
        CustomerDto GetCustomerById(int id);

        /// <summary>
        /// Registers (Creates) Customer
        /// </summary>
        ///  /// <returns>
        /// <see cref="CustomerDto"/>
        /// </returns>
        CustomerDto Register(string mail);

        /// <summary>
        /// Creates Customer
        /// </summary>
        void CreateCustomer(CustomerDto customer);

        /// <summary>
        /// Updates Customer
        /// </summary>
        void UpdateCustomer(int id, CustomerDto customer);

        /// <summary>
        /// Deletes Customer
        /// </summary>
        void DeleteCustomer(int id);

        /// <summary>
        /// Logins Customer
        /// </summary>
        ///  /// <returns>
        /// <see cref="CustomerDto"/> with corresponding mail
        /// </returns>
        CustomerDto Login(string mail);
    }
}