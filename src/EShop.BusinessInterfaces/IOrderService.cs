﻿using System.Collections.Generic;
using EShop.BusinessModels;
using EShop.BusinessModels.Enums;

namespace EShop.BusinessInterfaces
{
    /// <summary>
    /// Interface for Order services 
    /// </summary>
    public interface IOrderService
    {
        /// <summary>
        /// Gets all Orders
        /// </summary>
        ///  /// <returns>
        /// <see cref="IEnumerable{OrderDto}"/>
        /// </returns>
        IEnumerable<OrderDto> GetAllOrders();

        /// <summary>
        /// Gets order by Id
        /// </summary>
        ///  /// <returns>
        /// <see cref="OrderDto"/>
        /// </returns>
        OrderDto GetOrderById(int id);

        /// <summary>
        /// Creates Order
        /// </summary>
        void CreateOrder(ProductDto productDto, CustomerDto customerDto);

        /// <summary>
        /// Sets order's status
        /// </summary>
        void SetOrderStatus(OrderDto orderDto, OrderStateEnum state);

        /// <summary>
        /// Updates Order
        /// </summary>
        void UpdateOrder(int id, OrderDto customer);

        /// <summary>
        /// Deletes order
        /// </summary>
        void DeleteOrder(int id);
    }
}