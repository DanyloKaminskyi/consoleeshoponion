﻿using System.Collections.Generic;
using EShop.BusinessModels;

namespace EShop.BusinessInterfaces
{
    /// <summary>
    /// Interface for Product services 
    /// </summary>
    public interface IProductService
    {
        /// <summary>
        /// Gets products by name
        /// </summary>
        ///  /// <returns>
        /// <see cref="IEnumerable{ProductDto}"/>
        /// </returns>
        IEnumerable<ProductDto> GetProductsByName(string productName);

        /// <summary>
        /// Gets all products
        /// </summary>
        ///  /// <returns>
        /// <see cref="IEnumerable{ProductDto}"/>
        /// </returns>
        IEnumerable<ProductDto> GetAllProducts();

        /// <summary>
        /// Gets product by Id
        /// </summary>
        ///  /// <returns>
        /// <see cref="ProductDto"/>
        /// </returns>
        ProductDto GetProductById(int id);

        /// <summary>
        /// Creates Product
        /// </summary>
        void CreateProduct(ProductDto product);

        /// <summary>
        /// Updates Product
        /// </summary>
        void UpdateProduct(int id, ProductDto product);

        /// <summary>
        /// Deletes Product
        /// </summary>
        void DeleteProduct(int id);

    }
}