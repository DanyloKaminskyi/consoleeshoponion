﻿using System;
using System.Linq;
using EShop.Dialog;

namespace EShop.View
{
    class Program
    {
        private static void Main(string[] args)
        {
            Console.WriteLine("Welcome to the EShop");
            Dialog.Dialog dialog = new Dialog.Dialog();
            dialog.ProcessCompleted += bl_ProcessCompleted;
            foreach (int command in Enum.GetValues(typeof(CommandEnum)))
            {
                Console.WriteLine($"{Enum.GetName(typeof(CommandEnum), command)} - {command}");
            }

            while (true)
            {
                string input = Console.ReadLine();
                if (!input.All(char.IsDigit))
                {
                    Console.WriteLine("Only digits are accepted");
                    continue;
                }
                int command = Convert.ToInt16(input);
                if (!Enum.IsDefined(typeof(CommandEnum), command))
                {
                    Console.WriteLine("There is no such command");
                    continue;
                };

                switch (command)
                {
                    case (int)CommandEnum.Products:
                        {
                            var items = dialog.GetAllProducts();
                            foreach (var item in items)
                            {
                                Console.WriteLine(item.ToString());
                            }
                            continue;
                        }
                    case (int)CommandEnum.FindProduct:
                        {
                            Console.Write("Enter a product's name ");
                            var items = dialog.GetProductByName(Console.ReadLine());
                            foreach (var item in items)
                            {
                                Console.WriteLine(item.ToString());
                            }
                            continue;
                        }
                    case (int)CommandEnum.Login:
                        {
                            Console.Write("Enter your mail: ");
                            dialog.Login(Console.ReadLine());
                            continue;
                        }
                    case (int)CommandEnum.Logout:
                        {
                            dialog.Logout();
                            continue;
                        }
                    case (int)CommandEnum.Register:
                        {
                            Console.Write("Enter your mail: ");
                            dialog.Register(Console.ReadLine());
                            continue;
                        }
                    case (int)CommandEnum.MakeOrder:
                        {
                            Console.Write("Enter id of the product you want to order: ");
                            dialog.CreateOrder(Console.ReadLine());
                            continue;
                        }
                    case (int)CommandEnum.ConfirmOrder:
                        {
                            dialog.ConfirmOrder();
                            continue;
                        }
                    case (int)CommandEnum.CancelOrder:
                        {
                            dialog.CancelOrder();
                            continue;
                        }
                    case (int)CommandEnum.ViewOrderHistory:
                        {
                            var orders = dialog.ViewOrderHistory();
                            foreach (var item in orders)
                            {
                                Console.WriteLine(item);
                            }
                            continue;
                        }
                    case (int)CommandEnum.ChangePersonalData:
                        {
                            Console.Write("Enter your new Email: ");
                            dialog.ChangePersonalData(Console.ReadLine());
                            continue;
                        }
                    case (int)CommandEnum.ChangeUserData:
                        {
                            Console.Write("Enter id of the user you want to change: ");
                            string id = Console.ReadLine();
                            Console.Write("Enter new email for user: ");
                            string mail = Console.ReadLine();
                            dialog.ChangeUserData(id, mail);
                            continue;
                        }
                    case (int)CommandEnum.ChangeOrderStatus:
                        {
                            Console.Write("Enter id of the order: ");
                            string id = Console.ReadLine();
                            Console.Write("Enter new status for order: ");
                            string status = Console.ReadLine();
                            dialog.ChangeOrderStatus(id, status);
                            continue;
                        }

                    default: break;
                }
            }
        }

        public static void bl_ProcessCompleted(object sender, ProcessEventArgs e)
        {
            Console.WriteLine("Process " + (e.IsSuccessful ? "Completed Successfully" : $"failed: {e.ErrorMessage}"));
        }
    }
}
