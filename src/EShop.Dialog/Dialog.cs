﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EShop.BusinessInterfaces;
using EShop.BusinessModels;
using EShop.BusinessModels.Enums;
using EShop.Repo;
using EShop.Service;

namespace EShop.Dialog
{
    /// <summary>
    /// Class that segregates logic from view 
    /// </summary>
    public class Dialog
    {
        /// <summary>
        /// Object that imitates the db connection
        /// </summary>
        private static  readonly ShopDbContext _context = new ShopDbContext();

        /// <summary>
        /// Object of <see cref="ICustomerService"/> to use its methods
        /// </summary>
        private readonly ICustomerService _customerService = new CustomerService(new MockCustomerRepo(_context),new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new AutoMapperProfile());
        }).CreateMapper());

        /// <summary>
        /// Object of <see cref="IProductService"/> to use its methods
        /// </summary>
        private readonly IProductService _productService = new ProductService(new MockProductRepo(_context), new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new AutoMapperProfile());
        }).CreateMapper());


        /// <summary>
        /// Object of <see cref="IOrderService"/> to use its methods
        /// </summary>
        private readonly IOrderService _orderService = new OrderService(new MockOrderRepo(_context), new MapperConfiguration(cfg =>
        {
            cfg.AddProfile(new AutoMapperProfile());
        }).CreateMapper());

        /// <summary>
        /// EventHandler to delegate task states to view
        /// </summary>
        public event EventHandler<ProcessEventArgs> ProcessCompleted;

        /// <summary>
        /// <see cref="CustomerDto"/> that is currently using the application
        /// </summary>
        private CustomerDto _currentCustomer = new CustomerDto();

        /// <summary>
        /// Default constructor
        /// </summary>
        public Dialog()
        {
            _currentCustomer.Role = RoleEnum.Guest;
        }

        /// <summary>
        /// Gets all Products
        /// </summary>
        ///  /// <returns>
        /// <see cref="List{T}"/> of strings
        /// </returns>
        public List<string> GetAllProducts()
        {
            List<string> products = new List<string>();
            foreach (var item in _productService.GetAllProducts())
            {
                products.Add(item.ToString());
            }

            return products;
        }

        /// <summary>
        /// Gets all Products with the corresponding name
        /// </summary>
        ///  /// <returns>
        /// <see cref="List{T}"/> of strings
        /// </returns>
        public List<string> GetProductByName(string name)
        {
            List<string> products = new List<string>();
            try
            {
                foreach (var item in _productService.GetProductsByName(name))
                {
                    products.Add(item.ToString());
                }
            }
            catch
            {
                OnProcessCompleted(new ProcessEventArgs("There is no such product"));
            }

            return products;
        }
        /// <summary>
        /// Sets current customer to new, with the corresponding credentials. Accessible only by <see cref="RoleEnum.Guest"/> role or lower
        /// </summary>
        public void Login(string mail)
        {
            if (_currentCustomer.Role > RoleEnum.Guest)
            {
                OnProcessCompleted(new ProcessEventArgs("You must logout in order to login"));
                return;
            }

            try
            {
                _currentCustomer = _customerService.Login(mail);
                OnProcessCompleted(new ProcessEventArgs());
            }
            catch
            {
                OnProcessCompleted(new ProcessEventArgs("There is no such user"));
            }
        }
        /// <summary>
        /// Creates new Customer. Accessible only by <see cref="RoleEnum.Guest"/> role or lower
        /// </summary>
        public void Register(string mail)
        {
            if (_currentCustomer.Role > RoleEnum.Guest)
            {
                OnProcessCompleted(new ProcessEventArgs("You must logout to register a new accout"));
                return;
            }

            try
            {
                _currentCustomer = _customerService.Register(mail);
                OnProcessCompleted(new ProcessEventArgs());
            }
            catch
            {
                OnProcessCompleted(new ProcessEventArgs("Something went wrong"));
            }
        }
        /// <summary>
        /// Creates new Order for current User. Accessible only by <see cref="RoleEnum.User"/> role or higher
        /// </summary>
        public void CreateOrder(string productId)
        {
            if (_currentCustomer.Role < RoleEnum.User)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (productId.All(char.IsDigit))
            {
                try
                {
                    _orderService.CreateOrder(_productService.GetProductById(Convert.ToInt32(productId)),
                        _currentCustomer);
                    OnProcessCompleted(new ProcessEventArgs());
                }
                catch
                {
                    OnProcessCompleted(new ProcessEventArgs("There is no product with such id"));
                }
            }
            else
            {
                OnProcessCompleted(new ProcessEventArgs("Id must contain only digits"));
            }
        }
        /// <summary>
        /// Cancels Order. Accessible only by <see cref="RoleEnum.User"/> role or higher
        /// </summary>
        public void CancelOrder()
        {
            if (_currentCustomer.Role < RoleEnum.User)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (_currentCustomer.Orders is null)
            {
                OnProcessCompleted(new ProcessEventArgs("there are no orders to cancel"));
                return;
            }

            {
                _orderService.SetOrderStatus(_currentCustomer.Orders.Last(), OrderStateEnum.CancelledByUser);
                OnProcessCompleted(new ProcessEventArgs());
            }
        }

        /// <summary>
        /// Confirms Order. Accessible only by <see cref="RoleEnum.User"/> role or higher
        /// </summary>
        public void ConfirmOrder()
        {
            if (_currentCustomer.Role < RoleEnum.User)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (_currentCustomer.Orders is null)
            {
                OnProcessCompleted(new ProcessEventArgs("there are no orders to confirm"));
                return;
            }

            if (_currentCustomer.Orders.Last().Status is OrderStateEnum.CancelledByUser ||
                _currentCustomer.Orders.Last().Status is OrderStateEnum.CancelledByAdmin)
            {
                OnProcessCompleted(
                    new ProcessEventArgs("Sorry, your order has been canccelled. Please make a new one"));
                return;
            }

            {
                _orderService.SetOrderStatus(_currentCustomer.Orders.Last(), OrderStateEnum.Finalized);
                OnProcessCompleted(new ProcessEventArgs());
            }
        }

        /// <summary>
        /// Gets all Orders for current Customer
        /// </summary>
        ///  /// <returns>
        /// <see cref="List{T}"/> of strings
        /// </returns>
        public List<string> ViewOrderHistory()
        {
            var order = new List<string>();
            if (_currentCustomer.Role < RoleEnum.User)
            {
                order.Add("You are not permitted to use this command");
                return order;
            }

            if (_currentCustomer.Orders is null)
            {
                order.Add("You are not permitted to use this command");
                return order;
            }

            foreach (var item in _currentCustomer.Orders)
            {
                order.Add(item.Product.ToString());
            }
            OnProcessCompleted(new ProcessEventArgs());
            return order;
        }

        /// <summary>
        /// Changes mail of current Customer
        /// </summary>
        public void ChangePersonalData(string mail)
        {
            if (_currentCustomer.Role < RoleEnum.User)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (mail.Equals(String.Empty))
            {
                OnProcessCompleted(new ProcessEventArgs("You can't change email to empty string"));
                return;
            }
            _currentCustomer.Email = mail;
            _customerService.UpdateCustomer(_currentCustomer.Id, _currentCustomer);
            OnProcessCompleted(new ProcessEventArgs());
        }

        /// <summary>
        /// Changes data of any User. Accessible only by <see cref="RoleEnum.Administrator"/> and higher
        /// </summary>
        public void ChangeUserData(string id, string mail)
        {
            if (_currentCustomer.Role < RoleEnum.Administrator)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (mail.Equals(String.Empty) || !id.All(char.IsDigit))
            {
                OnProcessCompleted(new ProcessEventArgs("Wrong input data"));
                return;
            }

            try
            {
                var customer = _customerService.GetCustomerById(Convert.ToInt32(id));
                customer.Email = mail;
                _customerService.UpdateCustomer(customer.Id, customer);
                OnProcessCompleted(new ProcessEventArgs());
            }
            catch
            {
                OnProcessCompleted(new ProcessEventArgs("Something went wrong"));
            }
        }

        /// <summary>
        /// Changes order status. Accessible only by <see cref="RoleEnum.Administrator"/> and higher
        /// </summary>
        public void ChangeOrderStatus(string id, string status)
        {
            if (_currentCustomer.Role < RoleEnum.Administrator)
            {
                OnProcessCompleted(new ProcessEventArgs("You are not permitted to use this command"));
                return;
            }

            if (!id.All(char.IsDigit))
            {
                OnProcessCompleted(new ProcessEventArgs("Wrong input data"));
                return;
            }

            bool isGoodStatus = false;
            foreach (int order in Enum.GetValues(typeof(OrderStateEnum)))
            {
                if (status.Equals(Enum.GetName(typeof(OrderStateEnum), order)))
                {
                    isGoodStatus = true;
                    break;
                }
            }

            if (!isGoodStatus)
            {
                OnProcessCompleted(new ProcessEventArgs("There is no such status"));
                return;
            }

            try
            {
                var order = _orderService.GetOrderById(Convert.ToInt32(id));
                order.Status = (OrderStateEnum)Enum.Parse(typeof(OrderStateEnum), status);
                _orderService.UpdateOrder(order.Id, order);
                OnProcessCompleted(new ProcessEventArgs());
            }
            catch
            {
                OnProcessCompleted(new ProcessEventArgs("Something went wrong"));
            }
        }

        /// <summary>
        /// Releases the current Customer object and puts a new Customer with Guest Role in his place. Accessible only by <see cref="RoleEnum.User"/> or higher
        /// </summary>
        public void Logout()
        {
            if (_currentCustomer.Role < RoleEnum.User)
            {
                OnProcessCompleted(new ProcessEventArgs("You must login first in order to logout"));
                return;
            }
            _currentCustomer = new CustomerDto();
            _currentCustomer.Role = RoleEnum.Guest;
            OnProcessCompleted(new ProcessEventArgs());
        }

        /// <summary>
        /// Method to delegate <see cref="ProcessEventArgs"/> info to View
        /// </summary>
        protected virtual void OnProcessCompleted(ProcessEventArgs e)
        {
            ProcessCompleted?.Invoke(this, e);
        }
    }
}