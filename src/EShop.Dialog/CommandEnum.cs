﻿namespace EShop.Dialog
{
   
    public enum CommandEnum
    {
        Products,
        FindProduct,
        Register,
        Login,
        Logout,
        MakeOrder,
        CancelOrder,
        ConfirmOrder,
        ViewOrderHistory,
        ChangePersonalData,
        ChangeUserData,
        ChangeOrderStatus
    }
}