﻿using System;

namespace EShop.Dialog
{
    /// <summary>
    /// Arguments for Event in <see cref="Dialog"/>
    /// </summary>
    public class ProcessEventArgs : EventArgs
    {
        /// <summary>
        /// Contains information on the state of the task
        /// </summary>
        public bool IsSuccessful { get; set; }

        /// <summary>
        /// Contains information on what went wrong if the task failed
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// Basic constructor, used for fast <see cref="ProcessEventArgs"/> creation when Task was completed successfully
        /// </summary>
        public ProcessEventArgs()
        {
            IsSuccessful = true;
            ErrorMessage = "None";
        }

        /// <summary>
        /// Basic constructor, used for fast <see cref="ProcessEventArgs"/> creation when Task failed
        /// </summary>
        public ProcessEventArgs(string error)
        {
            IsSuccessful = false;
            ErrorMessage = error;
        }
    }
}