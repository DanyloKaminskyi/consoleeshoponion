﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EShop.BusinessInterfaces;
using EShop.BusinessModels;
using EShop.BusinessModels.Enums;
using EShop.Data;
using EShop.Repo;

namespace EShop.Service
{
    /// <summary>
    /// Service that allows to perform CRUD and other operations on <see cref="Customer"/> entities
    /// </summary>
    public class CustomerService : ICustomerService
    {
        /// <summary>
        /// contains implementation of  <see cref="IRepository{T}"/> interface 
        /// </summary>
        private readonly IRepository<Customer> _repository;

        /// <summary>
        /// contains implementation of  <see cref="IMapper"/> interface 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor with Dependency Injection of  <see cref="IRepository{T}"/> and <see cref="IMapper"/>
        /// </summary>
        public CustomerService(IRepository<Customer> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all <see cref="Customer"/> entities from corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{CustomerDto}"/> 
        /// </returns>
        public IEnumerable<CustomerDto> GetAllCustomers()
        {
            var customers = _repository.GetAll();
            
            return _mapper.Map<IEnumerable<CustomerDto>>(customers);
        }
        /// <summary>
        /// Gets <see cref="Customer"/>entity by ID from corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="CustomerDto"/> 
        /// </returns>
        public CustomerDto GetCustomerById(int id)
        {
            var customer = _repository.GetById(id);
            if (customer is null)
            {
                throw new ArgumentException("No Customer with such Id exists", nameof(id));
            }

            return _mapper.Map<CustomerDto>(customer);
        }

        /// <summary>
        /// Creates (Registers) <see cref="Customer"/> in the corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        public CustomerDto Register(string mail)
        {
            CustomerDto customer = new CustomerDto();
            customer.Role = RoleEnum.User;
            customer.Email = mail;
            _repository.Create(_mapper.Map<Customer>(customer));
            return customer;
        }
        /// <summary>
        /// Creates <see cref="Customer"/> in the corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        public void CreateCustomer(CustomerDto customer)
        {
            _repository.Create(_mapper.Map<Customer>(customer));

        }
        /// <summary>
        /// Updates <see cref="Customer"/> entity in  corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        public void UpdateCustomer(int id, CustomerDto customer)
        {
            var customerFromRepo = _repository.GetById(id);
            if (customerFromRepo == null)
            {
                throw new ArgumentException();
            }

            _repository.Update(id, _mapper.Map<Customer>(customer));
        
        }
        /// <summary>
        /// Deletes <see cref="Customer"/> entity from corresponding <see cref="IRepository{Customer}"/> repository
        /// </summary>
        public void DeleteCustomer(int id)
        {
            var customerFromRepo = _repository.GetById(id);
            if (customerFromRepo == null)
            {
                throw new ArgumentException();
            }
            _repository.Delete(customerFromRepo);
        }

        /// <summary>
        /// Logins <see cref="Customer"/> entity. Basically, returns customer entity by name
        /// </summary>
        /// /// <returns>
        /// <see cref="CustomerDto"/> 
        /// </returns>
        public CustomerDto Login(string mail)
        {
            var customersFromRepo = _repository.GetAll();
            if (customersFromRepo == null)
            {
                throw new ArgumentException();
            }

            if (customersFromRepo.Any(x => x.Email.Equals(mail)))   
            {
               var customer =  customersFromRepo.First(x => x.Email.Equals(mail));
               return _mapper.Map<CustomerDto>(customer);
            }
                

            throw new ArgumentException();
        }
    }
}