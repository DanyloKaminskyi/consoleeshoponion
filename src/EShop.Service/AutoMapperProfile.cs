﻿using AutoMapper;
using EShop.BusinessModels;
using EShop.Data;

namespace EShop.Service
{
    /// <summary>
    /// Profile that holds logic for data mapping between DTO and Entities
    /// </summary>
    public class AutoMapperProfile : Profile
    {
        /// <summary>
        /// Default constructor that holds logic for data mapping of the profile
        /// </summary>
        public AutoMapperProfile()
        {
            CreateMap<ProductDto, Product>().ReverseMap();

            CreateMap<CustomerDto, Customer>().ReverseMap();

            CreateMap<OrderDto, Order>().ReverseMap();

        }
    }
}