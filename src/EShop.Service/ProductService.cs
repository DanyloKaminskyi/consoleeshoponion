﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using EShop.BusinessInterfaces;
using EShop.BusinessModels;
using EShop.Data;
using EShop.Repo;

namespace EShop.Service
{
    /// <summary>
    /// Service that allows to perform CRUD and other operations on <see cref="Product"/> entities
    /// </summary>
    public class ProductService :IProductService
    {
        /// <summary>
        /// contains implementation of  <see cref="IRepository{T}"/> interface 
        /// </summary>
        private readonly IRepository<Product> _repository;

        /// <summary>
        /// contains implementation of  <see cref="IMapper"/> interface 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor with Dependency Injection of  <see cref="IRepository{T}"/> and <see cref="IMapper"/>
        /// </summary>
        public ProductService(IRepository<Product> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all product entities by name corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{ProductDto}"/> 
        /// </returns>
        public IEnumerable<ProductDto> GetProductsByName(string productName)
        {
            var products = _repository.GetAll();
            var searchResult = products.Where(x => x.Name.Equals(productName));
            return _mapper.Map<IEnumerable<ProductDto>>(searchResult);
        }

        /// <summary>
        /// Gets all <see cref="Product"/> entities from corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{ProductDto}"/> 
        /// </returns>
        public IEnumerable<ProductDto> GetAllProducts()
        {
            var products = _repository.GetAll();
            return _mapper.Map<IEnumerable<ProductDto>>(products);
        }

        /// <summary>
        /// Gets <see cref="Product"/> entity by ID from corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="ProductDto"/> 
        /// </returns>
        public ProductDto GetProductById(int id)
        {
            var product = _repository.GetById(id);
            if (product is null)
            {
                throw new ArgumentException();
            }

            return _mapper.Map<ProductDto>(product);
        }

        /// <summary>
        /// Creates <see cref="Product"/> in the corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>

        public void CreateProduct(ProductDto product)
        {
            _repository.Create(_mapper.Map<Product>(product));
        
        }
        /// <summary>
        /// Updates <see cref="Product"/> entity in  corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>
        public void UpdateProduct(int id, ProductDto product)
        {
            var productFromRepo = _repository.GetById(id);
            if (productFromRepo == null)
            {
                throw new ArgumentException();
            }

            _repository.Update(id, _mapper.Map<Product>(product));
           
        }

        /// <summary>
        /// Deletes <see cref="Product"/> entity from corresponding <see cref="IRepository{Product}"/> repository
        /// </summary>
        public void DeleteProduct(int id)
        {
            var productFromRepo = _repository.GetById(id);
            if (productFromRepo == null)
            {
                throw new ArgumentException();
            }
            _repository.Delete(productFromRepo);
      
        }
    }

}