﻿using System;
using System.Collections.Generic;
using AutoMapper;
using EShop.BusinessInterfaces;
using EShop.BusinessModels;
using EShop.BusinessModels.Enums;
using EShop.Data;
using EShop.Repo;

namespace EShop.Service
{
    /// <summary>
    /// Service that allows to perform CRUD and other operations on <see cref="Order"/> entities
    /// </summary>
    public class OrderService :IOrderService
    {
        /// <summary>
        /// contains implementation of  <see cref="IRepository{T}"/> interface 
        /// </summary>
        private readonly IRepository<Order> _repository;

        /// <summary>
        /// contains implementation of  <see cref="IMapper"/> interface 
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Constructor with Dependency Injection of  <see cref="IRepository{T}"/> and <see cref="IMapper"/>
        /// </summary>
        public OrderService(IRepository<Order> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Gets all <see cref="Order"/> entities from corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="IEnumerable{OrderDto}"/> 
        /// </returns>
        public IEnumerable<OrderDto> GetAllOrders()
        {
            var orders = _repository.GetAll();
            return _mapper.Map<IEnumerable<OrderDto>>(orders); 
        }

        /// <summary>
        /// Gets <see cref="Order"/> entity by ID from corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        /// /// <returns>
        /// <see cref="OrderDto"/> 
        /// </returns>
        public OrderDto GetOrderById(int id)
        {
            var order = _repository.GetById(id);
            if (order is null)
            {
                throw new ArgumentException();
            }

            return _mapper.Map<OrderDto>(order);
        }

        /// <summary>
        /// Creates <see cref="Order"/> in the corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        public void CreateOrder(ProductDto productDto, CustomerDto customerDto)
        {
           var orderDto = new OrderDto();
           orderDto.Customer = customerDto;
           orderDto.Product = productDto;
           orderDto.Status = OrderStateEnum.New;

           
                customerDto.Orders.Add(orderDto);
                _mapper.Map<Customer>(customerDto);
                _repository.Create(_mapper.Map<Order>(orderDto));
                
        }

        /// <summary>
        /// Updates <see cref="Order"/>' entity status in  corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        public void SetOrderStatus(OrderDto orderDto, OrderStateEnum state)
        {
            orderDto.Status = state;
            _repository.Update(orderDto.Id, _mapper.Map<Order>(orderDto));
        }
        /// <summary>
        /// Updates <see cref="Order"/> entity in  corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        public void UpdateOrder(int id, OrderDto orderDto)
        {
            var orderFromRepo = _repository.GetById(id);
            if (orderFromRepo == null)
            {
                throw new ArgumentException();
            }

            _repository.Update(id, _mapper.Map<Order>(orderDto));
           
        }
        /// <summary>
        /// Deletes <see cref="Order"/> entity from corresponding <see cref="IRepository{Order}"/> repository
        /// </summary>
        public void DeleteOrder(int id)
        {
            var orderFromRepo = _repository.GetById(id);
            if (orderFromRepo == null)
            {
                throw new ArgumentException();
            }
            _repository.Delete(orderFromRepo);
        }
    }
}