﻿using EShop.BusinessModels.Enums;

namespace EShop.BusinessModels
{
    public class OrderDto
    {
        public int Id { get; set; } 
        public OrderStateEnum Status { get; set; }
        public ProductDto Product { get; set; }

        public CustomerDto Customer { get; set; }
        public override string ToString()
        {
            return $"Id : {Id}, State: {Status}, CustomerId: {Customer.Id} ";
        }
    }
}   