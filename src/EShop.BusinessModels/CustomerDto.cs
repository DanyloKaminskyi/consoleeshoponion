﻿using System.Collections.Generic;
using EShop.BusinessModels.Enums;

namespace EShop.BusinessModels
{
    public class CustomerDto
    {
        public int Id { get; set; }
        public string Email { get; set; }

        public RoleEnum Role { get; set; }

        public ICollection<OrderDto> Orders { get; set; }

        public CustomerDto()
        {
            Orders = new List<OrderDto>();
        }
        public override string ToString()
        {
            return $"Id : {Id}, Email: {Email}, Role: {Role.ToString()} ";
        }
    }
}