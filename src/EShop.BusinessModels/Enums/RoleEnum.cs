﻿namespace EShop.BusinessModels.Enums
{
    public enum RoleEnum
    {
        Guest,
        User,
        Administrator
    }
}