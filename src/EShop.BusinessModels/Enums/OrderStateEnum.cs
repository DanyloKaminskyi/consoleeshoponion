﻿namespace EShop.BusinessModels.Enums
{
    public enum OrderStateEnum
    {
        New,
        CancelledByAdmin,
        CancelledByUser,
        PaymentPending,
        Sent,
        Received,
        Finalized
    }
}