﻿using System.ComponentModel.DataAnnotations;

namespace EShop.Data
{
    public class BaseEntity
    {
        [Key]
        [Required]
        public int Id { get; set; }
    }
}