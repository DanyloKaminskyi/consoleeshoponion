﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel.DataAnnotations;
using EShop.Data.Enums;

namespace EShop.Data
{
    public class Customer : BaseEntity
    {
        [MaxLength(250)]
        public string Email { get; set; }

        [Required]
        public RoleEnum Role { get; set; }

        public ICollection<Order> Orders { get; set; }

        public Customer()
        {
            Orders = new Collection<Order>();
        }
        public override string ToString()
        {
            return $"Id : {Id}, Email: {Email}, Role: {Role.ToString()} ";
        }
    }
}