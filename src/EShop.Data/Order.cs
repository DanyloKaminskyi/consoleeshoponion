﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using EShop.Data.Enums;

namespace EShop.Data
{
    public class Order : BaseEntity
    {
        [Required]
        public OrderStateEnum Status { get; set; }

        [Required]
        public Product Product { get; set; }
        [Required]
        public Customer Customer { get; set; }  

        
    }
}