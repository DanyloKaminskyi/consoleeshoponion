﻿using System.ComponentModel.DataAnnotations;

namespace EShop.Data
{
    public class Product : BaseEntity
    {
        [Required]
        [MaxLength(250)]
        public string Name { get; set; }

        public override string ToString()
        {
            return $"Id : {Id}, Name: {Name} ";
        }
    }
}