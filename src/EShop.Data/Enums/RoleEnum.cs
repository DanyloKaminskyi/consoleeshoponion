﻿namespace EShop.Data.Enums
{
    public enum RoleEnum
    {
        Guest,
        User,
        Administrator
    }
}