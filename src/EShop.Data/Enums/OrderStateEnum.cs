﻿namespace EShop.Data.Enums
{
    public enum OrderStateEnum
    {
        New,
        CancelledByAdmin,
        CancelledByUser,
        PaymentPending,
        Sent,
        Received,
        Finalized
    }
}