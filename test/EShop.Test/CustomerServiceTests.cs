using System;
using System.Collections.Generic;
using AutoMapper;
using EShop.BusinessModels;
using EShop.BusinessModels.Enums;
using EShop.Data;
using EShop.Data.Enums;
using EShop.Repo;
using EShop.Service;
using Moq;
using Xunit;

namespace EShop.Test
{
    public class CustomerServiceTests
    {
        private Mock<IRepository<Customer>> mockRepo;
        private IMapper configuration;
        

        public CustomerServiceTests()
        {
            mockRepo = new Mock<IRepository<Customer>>();
            configuration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfile()); }).CreateMapper();
        }

        [Fact]
        public void GetAllCustomers_ReturnsCorrectItem_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetCustomerList(1));
            var service = new CustomerService(mockRepo.Object,configuration);
            var actual = GetCustomerList(1);
            //Act
            var result = service.GetAllCustomers();
            //Assert
            Assert.Collection(result,
                item => Assert.Equal(1, item.Id)
            );
        }

        [Fact]
        public void GetAllCustomers_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetCustomerList(1));
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act
            var result = service.GetAllCustomers();
            //Assert
            Assert.IsType<List<CustomerDto>>(result);
        }

        [Fact]
        public void GetCustomersById_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(new Customer()
            {
                Id = 1,
                Email = "Test",
                Role = Data.Enums.RoleEnum.Guest
            });
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act
            var result = service.GetCustomerById(1);
            //Assert
            Assert.IsType<CustomerDto>(result);
        }

        [Fact]
        public void GetCustomersById_ThrowsArgumentException_WhenNoResources()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => service.GetCustomerById(1));
        }

        [Fact]
        public void Register_ReturnsCorrectType()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.Create(new Customer()
                {
                    Id = 1,
                    Email = "Test",
                    Role = Data.Enums.RoleEnum.User
                }));
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act
            var result = service.Register("Test");
            //Assert
            Assert.IsType<CustomerDto>(result);
        }

        [Fact]
        public void UpdateCustomer_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.Update(1, new Customer()
                {
                    Id = 1,
                    Email = "Test",
                    Role = Data.Enums.RoleEnum.User
                }));
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.UpdateCustomer(1, new CustomerDto()
            {
                Id = 1,
                Email = "Test",
                Role = BusinessModels.Enums.RoleEnum.User
            }));
        }

        [Fact]
        public void DeleteCustomer_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.DeleteCustomer(1));
        }

        [Fact]
        public void Login_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(() => null);
            var service = new CustomerService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.Login("Test"));
        }

        public List<Customer> GetCustomerList(int num)
        {
            var customers = new List<Customer>();
            if (num > 0) customers.Add(new Customer()
            {
                Id = 1,
                Email = "Test",
                Role = Data.Enums.RoleEnum.Guest
            });
            return customers;
        }
    }
}