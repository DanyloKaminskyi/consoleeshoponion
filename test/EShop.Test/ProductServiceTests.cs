using System;
using System.Collections.Generic;
using AutoMapper;
using EShop.BusinessModels;
using EShop.Data;
using EShop.Repo;
using EShop.Service;
using Moq;
using Xunit;

namespace EShop.Test
{
    public class ProductServiceTests
    {
        private Mock<IRepository<Product>> mockRepo;
        private IMapper configuration;

        public ProductServiceTests()
        {
            mockRepo = new Mock<IRepository<Product>>();
            configuration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfile()); }).CreateMapper();
        }

        [Fact]
        public void GetAllProducts_ReturnsCorrectItem_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetOrderList(1));
            var service = new ProductService(mockRepo.Object, configuration);
            var actual = GetOrderList(1);
            //Act
            var result = service.GetAllProducts();
            //Assert
            Assert.Collection(result,
                item => Assert.Equal(0, item.Id)
            );
        }

        [Fact]
        public void GetAllProducts_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetOrderList(1));
            var service = new ProductService(mockRepo.Object, configuration);
            //Act
            var result = service.GetAllProducts();
            //Assert
            Assert.IsType<List<ProductDto>>(result);
        }

        [Fact]
        public void GetProductsById_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(0)).Returns(new Product
            {
                Id = 0,
                Name = "Leather jacket",
            });
            var service = new ProductService(mockRepo.Object, configuration);
            //Act
            var result = service.GetProductById(0);
            //Assert
            Assert.IsType<ProductDto>(result);
        }

        [Fact]
        public void GetProductsById_ThrowsArgumentException_WhenNoResources()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new ProductService(mockRepo.Object, configuration);
            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => service.GetProductById(1));
        }

        [Fact]
        public void UpdateProduct_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.Update(0, new Product
                {
                    Id = 0,
                    Name = "Leather jacket",
                }));
            var service = new ProductService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.UpdateProduct(0, new ProductDto
            {
                Id = 0,
                Name = "Leather jacket",
            }));
        }

        [Fact]
        public void DeleteProduct_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new ProductService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.DeleteProduct(1));
        }

        public List<Product> GetOrderList(int num)
        {
            var products = new List<Product>();
            if (num > 0)
                products.Add(new Product
                {
                    Id = 0,
                    Name = "Leather jacket",
                });
            return products;
        }
    }
}