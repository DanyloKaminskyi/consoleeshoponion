using System;
using System.Collections.Generic;
using AutoMapper;
using EShop.BusinessModels;
using EShop.Data;
using EShop.Data.Enums;
using EShop.Repo;
using EShop.Service;
using Moq;
using Xunit;

namespace EShop.Test
{
    public class OrderServiceTests
    {
        private Mock<IRepository<Order>> mockRepo;
        private IMapper configuration;

        public OrderServiceTests()
        {
            mockRepo = new Mock<IRepository<Order>>();
            configuration = new MapperConfiguration(cfg => { cfg.AddProfile(new AutoMapperProfile()); }).CreateMapper();
        }

        [Fact]
        public void GetAllOrders_ReturnsCorrectItem_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetOrderList(1));
            var service = new OrderService(mockRepo.Object, configuration);
            var actual = GetOrderList(1);
            //Act
            var result = service.GetAllOrders();
            //Assert
            Assert.Collection(result,
                item => Assert.Equal(0, item.Id)
            );
        }

        [Fact]
        public void GetAllOrders_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetAll()).Returns(GetOrderList(1));
            var service = new OrderService(mockRepo.Object, configuration);
            //Act
            var result = service.GetAllOrders();
            //Assert
            Assert.IsType<List<OrderDto>>(result);
        }

        [Fact]
        public void GetOrdersById_ReturnsCorrectType_WhenHasOneResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(new Order
            {
                Id = 0,
                Status = OrderStateEnum.New,
                Product = 
                    new Product
                    {
                        Id = 0, Name = "Leather jacket",
                    }
            });
            var service = new OrderService(mockRepo.Object, configuration);
            //Act
            var result = service.GetOrderById(1);
            //Assert
            Assert.IsType<OrderDto>(result);
        }

        [Fact]
        public void GetOrdersById_ThrowsArgumentException_WhenNoResources()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new OrderService(mockRepo.Object, configuration);
            //Act
            //Assert
            Assert.Throws<ArgumentException>(() => service.GetOrderById(1));
        }

        [Fact]
        public void UpdateOrder_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.Update(0, new Order
                {
                    Id = 0,
                    Status = OrderStateEnum.New,
                    Product =
                    new Product
                    {
                    Id = 0,
                    Name = "Leather jacket",
                }
                }));
            var service = new OrderService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.UpdateOrder(1, new OrderDto
            {
                Id = 0,
                Status = BusinessModels.Enums.OrderStateEnum.New,
                Product =
                    new ProductDto
                    {
                        Id = 0,
                        Name = "Leather jacket",
                    }
            }));
        }

        [Fact]
        public void DeleteOrder_ThrowsArgumentException_WhenDBHasNoResource()
        {
            //Arrange
            mockRepo.Setup(repo =>
                repo.GetById(1)).Returns(() => null);
            var service = new OrderService(mockRepo.Object, configuration);
            //Act

            //Assert
            Assert.Throws<ArgumentException>(() => service.DeleteOrder(1));
        }

        public List<Order> GetOrderList(int num)
        {
            var orders = new List<Order>();
            if (num > 0)
                orders.Add(new Order
                {
                    Id = 0,
                    Status = OrderStateEnum.New,
                    Product =
                        new Product
                        {
                            Id = 0,
                            Name = "Leather jacket",
                        }
                });
            return orders;
        }
    }
}